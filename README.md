# Fork from https://github.com/slamach/react-native-todo-app
Harus menggunakan node 16.x (error jika menggunakan >=18.x) 
Sudah di tes dengan versi v16.13.0 dan berjalan dengan baik
Project ini masih menggunakan Expo SDK 44.0.0 (deprecated)
Dimana masih mewajibkan instalasi EXPO-CLI secara global

Untuk upgrade ke EXPO SDK 49, ikuti petunjuk pada blog ini (official):
https://blog.expo.dev/expo-sdk-49-c6d398cdf740

Untuk penggunaan EXPO SDK baru, akan disimpan di branch git expo49

# To Do (React Native)

<p align="center">
  <img src="doc/screenshot.png" width="300" alt="App Screenshot">
</p>

## About the Project

React Native To Do application created for learning purposes.

You can add tasks, delete them and mark as done.

### Built With

- Expo
- React Native
- Redux
- Redux Toolkit
- Redux Persist
- Husky
- Prettier

## Installation and Usage

```
npm install
```

To make the pre-commit hook work, you need to run `npm install` when the project is already initialized as a Git repository.

If the project is initialized as a repository later, you need to additionally execute `npm run prepare`.

```
npm start
```

## Contact

Dmitrii Sviridov  
Telegram: [slamach](https://t.me/slamach)  
Email: sviridov.dvv@gmail.com
